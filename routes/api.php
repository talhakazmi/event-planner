<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('jwt.auth')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/signup', 'Auth\RegisterController@signup');
Route::post('/signin', 'Auth\LoginController@signin');
//API calls with JWT Auth no call will entertain without token
Route::middleware('jwt.auth')->post('/signout', 'Auth\LoginController@signout');

Route::middleware('jwt.auth')->post('/event/add', 'EventController@add');
Route::middleware('jwt.auth')->get('/event/list', 'EventController@upcomingEvents');

Route::middleware('jwt.auth')->get('/event/{id}', 'EventController@get');
Route::middleware('jwt.auth')->post('/stand/add', 'StandController@add');
Route::middleware('jwt.auth')->post('/stand/{id}', 'StandController@update');
Route::middleware('jwt.auth')->post('/visitor', 'StandController@add_visitor');
Route::middleware('jwt.auth')->get('/email/{id}', 'StandController@send_visitor_to_admin');
//return visitors list by stand id
Route::middleware('jwt.auth')->get('/visitor/{id}', function($id){
	return App\VisitorLog::where('stand_id',$id)->get();
});
//get stand detail by stand id
Route::middleware('jwt.auth')->get('/stand/{id}', function($id){
	return App\Stand::findOrFail($id);
});
//get booked stands by company/user id 
Route::middleware('jwt.auth')->get('/stands/{id}', function($id){
	return App\Stand::where('company_id',$id)->get();
});
//create image or file path to display image or download file in case of Brochure
Route::get('/{public}/{image}/{file}', function($public,$image,$file){
    
	$path = storage_path() .'\\app\\'. $public.'\\'.$image.'\\'.$file;

    if(!File::exists($path)) abort(404);

    $file = File::get($path);
    $type = File::mimeType($path);
    $filename = File::name($path);
	$ext = File::extension($path);

	if(strstr($type,'application'))
	{
		return Response::download($path, $filename.'.'.$ext, [
            'Content-Length: '. filesize($path)
        ]);
	} else {
		$response = Response::make($file, 200);
		$response->header("Content-Type", $type);
	}

    return $response;

});
