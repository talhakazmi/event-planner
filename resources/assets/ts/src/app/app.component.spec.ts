import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { LocalStorageService, LocalStorageModule } from 'angular-2-local-storage';
import { AppComponent } from './app.component';

describe('AppComponent', () => {

  //Configure testing module with components, services and modules required for testing
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
	  providers:    [ LocalStorageService ],
	  //importing routerTestModule for router-outlet
	  //importing localstorage for localstorage usage in template
	  imports: [ 
		RouterTestingModule,
		LocalStorageModule.withConfig({
			prefix: "my-app",
			storageType: "localStorage"
		})
	  ]
    }).compileComponents();
  }));

  /*
  *	Test to verify app should be created
  */
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  /*
  *	Test to verify application should contain title element with text app in it
  */
  it(`should have as title 'app'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('app');
  }));

});
