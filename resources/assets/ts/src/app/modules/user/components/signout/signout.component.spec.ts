import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { Router } from '@angular/router';

import { SignoutComponent } from './signout.component';

import { LocalStorageService, LocalStorageModule } from 'angular-2-local-storage';

describe('SignoutComponent', () => {
  let component: SignoutComponent;
  let fixture: ComponentFixture<SignoutComponent>;
  
  //Configure testing module with components, services and modules required for testing
  beforeEach(async(() => {
	
    TestBed.configureTestingModule({
      declarations: [ SignoutComponent ],
	  providers: [
			LocalStorageService,
			{provide: Router, useClass: class { navigate = jasmine.createSpy("navigate"); }
			}
		],
	  imports: [ 
			LocalStorageModule.withConfig({
					prefix: "my-app",
					storageType: "localStorage"
				})
		]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignoutComponent);
    component = fixture.componentInstance;
  });

  /*
  *	Test to verify component should be created
  */
  it('should be created', () => {
    expect(component).toBeTruthy();
  });
  
  /*
  * Here we test the method and make sure we actually navigate
  */
  it('should navigate to /home when component initiate', () => {
	  
	  let router = fixture.debugElement.injector.get(Router);
	  //Place an SPY on ngOnInit method
	  spyOn(component, 'ngOnInit').and.callThrough();
	  
	  //call ngOnInit()
	  fixture.detectChanges();
	  //test navigation to be called to /home
	  expect(router.navigate).toHaveBeenCalledWith(['/home']);
	  //test ngOnInit method called
	  expect(component.ngOnInit).toHaveBeenCalled();
  });
});
