import { Component } from '@angular/core';
import { Router } from '@angular/router';
//importing user services class
import { UserService } from '../../user.service';
//importing local storage service
import { LocalStorageService } from 'angular-2-local-storage';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html'
})
export class SignupComponent {

  public user = {name:'', email:'', password:'', password_confirmation: ''};
  public errors = {};
  
  constructor(
		//user service to call methods to communicate from API
		private userService: UserService,
		//angular router class for navigation
		private router: Router,
		//local storage class to store user data
		private localStorage: LocalStorageService
	) { console.log(this.localStorage.get('token')); }
  
  /**
  *	login method calls on sign in form submit
  * get user from API, stroe information in local storage and navigate to map screen.
  **/
  save() {
	  this.userService.addUser(this.user).subscribe(
		//observable return in response navigate on succsse and console error in case of http error from server
		(response) => {
			if(response.status == "success") {
				this.localStorage.set('token',response.responseData.user.token);
				this.localStorage.set('id',response.responseData.user.id);
				this.localStorage.set('name',response.responseData.user.name);
				this.localStorage.set('email',response.responseData.user.email);
				this.localStorage.set('is_admin',response.responseData.user.is_admin);
				this.localStorage.set('created_at',response.responseData.user.created_at);
				
				this.router.navigate(['event/map']);
			} else {
				//service errors to display on signup form
				this.errors = response.error
			}
		},
		(error) => {
			console.log('=================Error=================');
			console.log(error);
		}
	  );
  }

}
