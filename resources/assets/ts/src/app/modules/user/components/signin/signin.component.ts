import { Component } from '@angular/core';
import { Router } from '@angular/router';
//importing user services class
import { UserService } from '../../user.service';
//importing local storage service
import {LocalStorageService} from 'angular-2-local-storage';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent {

  public user = {email:'', password:''};
  public errors = {};
  
  constructor(
		//user service to call methods to communicate from API
		private userService: UserService,
		//angular router class for navigation
		private router: Router,
		//local storage class to store user data
		private localStorage: LocalStorageService
	) { }
	
  /**
  *	login method calls on sign in form submit
  * get user from API, stroe information in local storage and navigate to map screen.
  **/
  login() {
	  this.userService.getUser(this.user).subscribe(
	  //observable return in response navigate on succsse and console error in case of http error from server
		(response) => {
			if(response.status == "success") {
				this.localStorage.set('token',response.responseData.token);
				this.localStorage.set('id',response.responseData.user.id);
				this.localStorage.set('name',response.responseData.user.name);
				this.localStorage.set('email',response.responseData.user.email);
				this.localStorage.set('is_admin',response.responseData.user.is_admin);
				this.localStorage.set('created_at',response.responseData.user.created_at);
				
				this.router.navigate(['event/map']);
			} else {
				//service errors to display on signin form
				this.errors = response.error
			}
		},
		(error) => {
			console.log('=================Error=================');
			console.log(error);
		}
	  );
  }
}
