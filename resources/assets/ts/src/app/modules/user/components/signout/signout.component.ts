import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//importing local storage service
import {LocalStorageService} from 'angular-2-local-storage';

@Component({
  selector: 'app-signout',
  templateUrl: './signout.component.html',
  styleUrls: ['./signout.component.css']
})

//implementation of interface OnInit class from angular core
export class SignoutComponent implements OnInit {

  constructor(
		private router: Router,
		private localStorage: LocalStorageService
	) { }

	//OnInit class method to be defined here, invoke on class initate
	ngOnInit() {
		this.localStorage.clearAll();
		this.router.navigate(['/home']);
	}
}
