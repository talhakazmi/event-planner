import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpModule, Response, XHRBackend, ResponseOptions } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { SigninComponent } from './signin.component';
import { UserService } from '../../user.service';

import { LocalStorageService, LocalStorageModule } from 'angular-2-local-storage';

describe('SigninComponent', () => {
  let component: SigninComponent;
  let fixture: ComponentFixture<SigninComponent>;
 
  //Configure testing module with components, services and modules required for testing
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SigninComponent ],
	  imports: [ 
			FormsModule,
			HttpModule,
			RouterTestingModule,
			LocalStorageModule.withConfig({
					prefix: "my-app",
					storageType: "localStorage"
				})
		],
	  providers: [
			UserService,
			LocalStorageService,
			{provide: XHRBackend, useClass: MockBackend}
		]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SigninComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*
  *	Test to verify component should be created
  */
  
  it('should be created', () => {
    expect(component).toBeTruthy();
  });
  
  /*
  *	getUser method in UserService get user data from API.
  */
  
  //Testing request and response in success case.
  it('Get user service should respond correctly', inject([UserService, XHRBackend], (UserService, mockBackend) => {
    //mock response to be expected from service
	const mockResponse = {
		status: 'success',
		responseData: {
			token: '',
			user: {				
				name: 'talhakazmi',
				email: 'talhakaz@gmail.com',
				password: '654321'
			}
		},
		error: ''
	}
	//mock request to pass valid data to the service for valid repsonse
	const mockRequest = {
		name: 'talhakazmi',
		email: 'talhakaz@gmail.com'
	}
	//create mock response to be returned from service
	mockBackend.connections.subscribe((connection)=>{
		connection.mockRespond( new Response( new ResponseOptions({
			body: JSON.stringify(mockResponse)
		})));
	});
	
	//service call to verify correct reponse returned from serice as expected
	UserService.getUser(mockRequest).subscribe((result)=>{
		expect(result).not.toBeNull();
		expect(result.status).toEqual('success');
		expect(result.responseData).not.toBeNull();
		expect(result.responseData.user.name).toEqual(mockRequest.name);
		expect(result.error).toEqual('');
	});
	
  }));
  
  //Testing request and response in error case.
  it('Get user service should respond error', inject([UserService, XHRBackend], (UserService, mockBackend) => {
    //mock response to be expected from service
	const mockResponse = {
		status: 'failed',
		responseData: '',
		error: 
		{
			email:['Invalid Credentials']
		}
	}
	//mock request to pass valid data to the service for valid repsonse
	const mockRequest = {
		name: '',
		email: ''
	}
	//create mock response to be returned from service
	mockBackend.connections.subscribe((connection)=>{
		connection.mockRespond( new Response( new ResponseOptions({
			body: JSON.stringify(mockResponse)
		})));
	});
	//service call to verify correct reponse returned from serice as expected
	UserService.getUser(mockRequest).subscribe((result)=>{
		expect(result).not.toBeNull();
		expect(result.status).toEqual('failed');
		expect(result.responseData).toEqual('');
		expect(result.error).not.toBeNull();
		expect(result.error.email[0]).toEqual('Invalid Credentials');
	});
	
  }));
});
