import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { CommonModule } from '@angular/common';
//Import user routing module
import { UserRoutingModule } from './user-routing.module';
//importing user services class
import { UserService } from './user.service';
//importing users components in module
import { SignoutComponent } from './components/signout/signout.component';
import { SigninComponent } from './components/signin/signin.component';
import { SignupComponent } from './components/signup/signup.component';

@NgModule({
  //loading modules in user module
  imports: [
    CommonModule,
	FormsModule,
	UserRoutingModule
  ],
  //loading components in module
  declarations: [
	SignoutComponent, 
	SigninComponent, 
	SignupComponent
  ],
  //loading service in module
  providers: [
	UserService
  ]
})
export class UserModule { }
