import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
//importing components to be used in routing
import { SignoutComponent } from './components/signout/signout.component';
import { SigninComponent } from './components/signin/signin.component';
import { SignupComponent } from './components/signup/signup.component';

//defining routes for user module.
const routes: Routes = [
  {
	//route match to user
    path: 'user',
	//children paths to user components
    children:[
      { path: 'signup', component: SignupComponent },
      { path: 'signin', component: SigninComponent },
      { path: 'signout', component: SignoutComponent }
    ]
  }
  
  
];

@NgModule({
  //importing given route
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class UserRoutingModule { }
