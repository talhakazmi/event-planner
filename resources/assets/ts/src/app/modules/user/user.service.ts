import { Injectable } from '@angular/core';
import { Http} from '@angular/http';
//importing rest client class for api calling
import { RestClientService } from '../../services/rest-client.service';
//importing local storage service
import { LocalStorageService } from 'angular-2-local-storage';

@Injectable()
export class UserService extends RestClientService {
	//User API urls for sign in and sign up
	GET_USER = "signin";
	ADD_USER = "signup";
  //passing http and localstorage property to rest client class
  constructor(http: Http, localStorage: LocalStorageService) {
    super(http, localStorage);
  }
  /**
  *	getUser method return user data on the basis of email and password
  * Params:
  *		{email:'', password:''}
  *	Return:
  *		{status: '', responseData: {token: '', user: {}}, error: ''}
  **/
  public getUser(params){
    return this.post(this.GET_USER,params);
  }

  /**
  *	addUser method send user data to the API to register user
  * Params:
  *		{name:'', email:'', password:'', password_confirmation: ''}
  *	Return:
  *		{status: '', responseData: {user: {token: '', ... }}, error: ''}
  **/
  public addUser(params){
    return this.post(this.ADD_USER,params);
  }
}
