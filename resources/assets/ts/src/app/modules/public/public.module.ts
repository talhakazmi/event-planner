import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//Import public routing module
import { PublicRoutingModule } from './public-routing.module';
//importing users components in module
import { HomeComponent } from './components/home/home.component';

@NgModule({
  //loading modules in public module
  imports: [
    CommonModule,
	PublicRoutingModule
  ],
  //loading components in module
  declarations: [HomeComponent]
})
export class PublicModule { }
