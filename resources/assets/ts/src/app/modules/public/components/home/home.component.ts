import { Component } from '@angular/core';
//import router class for navigation
import { Router } from '@angular/router';
//importing local storage service
import {LocalStorageService} from 'angular-2-local-storage';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  //local storage class to store user data
  constructor(
			//local storage class to store user data
			private localStorage: LocalStorageService,
			//angular router class for navigation
			private router: Router,
		) {
			if(this.localStorage.get('id') > 0){
				this.router.navigate(['event/map']);
			}
		}

}
