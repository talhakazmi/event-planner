import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LocalStorageService, LocalStorageModule } from 'angular-2-local-storage';

import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  //Configure testing module with components, services and modules required for testing
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeComponent ],
	  providers:    [ LocalStorageService ],
	  imports: [ 
		LocalStorageModule.withConfig({
			prefix: "my-app",
			storageType: "localStorage"
		})
	  ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*
  *	Test to verify component should be created
  */
  it('should be created', () => {
    expect(component).toBeTruthy();
  });
  
  /*
  *	Test to verify component should contain h1 tag with text welcome to app!
  */
  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(HomeComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to app!');
  }));
  
});
