import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
//importing components to be used in routing
import { HomeComponent } from './components/home/home.component';

//defining routes for public module.
const routes: Routes = [
  //route match to home will use HomeComponent
  {path: 'home', component: HomeComponent}
];

@NgModule({
  //importing given route
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class PublicRoutingModule { }
