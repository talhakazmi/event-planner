import { Component, Input } from '@angular/core';
import { EventService } from '../../event.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-stand',
  templateUrl: './stand.component.html',
  styleUrls: ['./stand.component.css']
})
export class StandComponent {

  /**
  * Input properties of component to get detail from parent component via component attribute 
  **/
  @Input() isAdmin:string;
  @Input() detail;
  @Input() row;
  @Input() column;
  @Input() event_id;
  
  //event service to call methods to communicate from API
  constructor(private eventService: EventService,) { }
  /**
  * method called on make available button
  * Param: $event
  **/
  addStand(e) {
	  // get values to store with stand
	  let params = {
				  price: $(e.target).parent().children('.input-group').children('.form-control').val(),
				  event_id: this.event_id,
				  cell_row: this.row,
				  cell_col: this.column
			  }
	  //store stand image file name and image file in array of object files
	  let files = [
				{
					'name': $(e.target).parent().children('.file-div').children('#preview-input').prop('name'),
					'file': $(e.target).parent().children('.file-div').children('#preview-input').prop('files')
				}
			];
	  //remove errors if any before calling service 
	  $(e.target).parent().children('.input-group').removeClass('has-error');
	  //If price is given call service to add stand in DB via API
	  if(params.price != '') {		  
		  this.eventService.addStand(params,files).subscribe(
			(response) => {
				
				$(e.target).parent().children('.input-group').children('.form-control').remove();
				$(e.target).parent().children('.input-group').children('.input-group-addon').html('$'+params.price);
				$(e.target).parent().children('.card-text').html('Stand Available');
				$(e.target).remove();
				
				//console.log(this.stand);
			},
			(error) => {
				console.log(error);
			}
		  );
	  } else {
		  $(e.target).parent().children('.input-group').addClass('has-error');
	  }
  }
  
}
