import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpModule, Response, XHRBackend, ResponseOptions } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { StandDetailComponent } from './stand-detail.component';

import { jsonObjectPipe } from '../../../../shared/pipes/ObjectLoopingPipe';

import { EventService } from '../../event.service';

import { LocalStorageService, LocalStorageModule } from 'angular-2-local-storage';

describe('StandDetailComponent', () => {
  let component: StandDetailComponent;
  let fixture: ComponentFixture<StandDetailComponent>;
  //Configure testing module with components, services and modules required for testing
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StandDetailComponent, jsonObjectPipe ],
      providers: [ 
				jsonObjectPipe,
				EventService,
				LocalStorageService,
				{ provide: XHRBackend, useClass: MockBackend }
			],
	  imports: [ 
			FormsModule,
			HttpModule,
			RouterTestingModule,
			LocalStorageModule.withConfig({
					prefix: "my-app",
					storageType: "localStorage"
				})
		]
    })
    .compileComponents();
  }));

  /*
  *	getStand method in EventService get stand data.
  */
  it('should be created', inject([EventService, XHRBackend], (EventService, mockBackend) => {
    //mock response to be expected from service
	const mockResponse = {
						id: 12,
						event_id: 4,
						cell_row: 0,
						cell_col: 0,
						company: 'company name',
						document: 'public/event_images/0OQIPlWy6AQStBPnZXt76wSX6L6mhhELBANZF8o6.doc',
						logo: 'public/event_images/P2ZrrgwFoOJCSWylGgXaEhFiJk2mnnu2Q6NI01wY.png',
						email: 'abc@company.com',
						price: 123,
						preview: 'public/event_images/OtHH1qiF2VNSV8uMEUWj6qAzr1a97ljDMCIuqfP6.jpeg',
						company_id: 9	
					};
	//create mock response to be returned from service
	mockBackend.connections.subscribe((connection) => {
		connection.mockRespond( new Response( new ResponseOptions({
			body: JSON.stringify(mockResponse)
		})));
	});
	//service call to verify correct reponse returned from service as expected
	EventService.getStand(12).subscribe((responseData)=>{
		expect(responseData.id).toEqual(mockResponse.id);
	});
	
	//expect(component).toBeTruthy();
  }));
});
