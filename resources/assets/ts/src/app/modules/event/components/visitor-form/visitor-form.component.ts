import { Component, OnInit } from '@angular/core';
import { EventService } from '../../event.service';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-visitor-form',
  templateUrl: './visitor-form.component.html',
  styleUrls: ['./visitor-form.component.css']
})
export class VisitorFormComponent implements OnInit {
	
  id: number = 0;
  visitor = {name:null,email:null,occupation:null,stand_id:null};
  error: string = '';
  success: string = '';
  visitors = [];
  
  constructor(
			//event service to call methods to communicate from API
			private eventService: EventService,
			//angular ActivatedRoute class for getting query string
			private route: ActivatedRoute
		) { }
  /**
  * on component initialize and get id from query string
  * fetch visitors list of stand id from API
  **/
  ngOnInit() {
	  this.route.params.subscribe((params) => this.id = params['id']);
	  this.eventService.getVisitors(this.id).subscribe(
		(response) => {
			this.visitors = response;
		},
		(error) => {
			console.log(error);
		}
	  );
  }
  
  /**
  *method called on add visitor button to add given visitor information to DB
  **/
  addVisitor() {
	  this.error = '';
	  this.success = '';
	  let params = {
				'name': this.visitor.name,
				'email': this.visitor.email,
				'occupation': this.visitor.occupation,
				'stand_id': this.id
			}
	  //AddVisitor POST service to pass visitor data to API
	  this.eventService.addVisitor(params).subscribe(
	  (response)=>{
		  if(response.status == 'success')
			//response.responseData.visitor
			this.success = 'Visitor added to the log';
		  else
			this.error = response.error;
	  },
	  (error)=>{
		  this.error = error;
	  });
  }
  
  /**
  * send email method invoke on button to send email to admin the list of visitors visit the stand
  * this function can be moved to queue and cron job to invoke on event end date via cron job and send email in a queue. (beanstalkd)
  **/
  sendEmail() {
	  this.eventService.emailLog(this.id).subscribe(
	  (response)=>{
		  console.log(response);
	  },
	  (error)=>{
		  console.log(error);
	  });
  }

}