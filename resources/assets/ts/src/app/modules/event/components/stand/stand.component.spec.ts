import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { HttpModule, Http, Response, XHRBackend, ResponseOptions } from '@angular/http';

import { StandComponent } from './stand.component';
import { EventService } from '../../event.service';

import { LocalStorageService, LocalStorageModule } from 'angular-2-local-storage';

describe('StandComponent', () => {
  let component: StandComponent;
  let fixture: ComponentFixture<StandComponent>;
	
  //Configure testing module with components, services and modules required for testing
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StandComponent ],
      providers: [ EventService, LocalStorageService ],
      imports: [ 
			HttpModule,
			LocalStorageModule.withConfig({
					prefix: "my-app",
					storageType: "localStorage"
				})
		]	  
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*
  *	Test to verify component should be created
  */
  it('should be created', inject([EventService], (EventService) => {
    expect(component).toBeTruthy();
  }));
});
