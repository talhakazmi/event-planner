import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { HttpModule, Response, XHRBackend, ResponseOptions } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { FormsModule } from '@angular/forms';
import { LocalStorageService, LocalStorageModule } from 'angular-2-local-storage';

import { AddEventFormComponent } from './add-event-form.component';
import { jsonObjectPipe } from '../../../../shared/pipes/ObjectLoopingPipe';

import { EventService } from '../../event.service';

describe('AddEventFormComponent', () => {
  let component: AddEventFormComponent;
  let fixture: ComponentFixture<AddEventFormComponent>;

  //Configure testing module with components, services and modules required for testing
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEventFormComponent, jsonObjectPipe ],
	  imports: [ 
			HttpModule,
			FormsModule,
			LocalStorageModule.withConfig({
					prefix: "my-app",
					storageType: "localStorage"
				})
		],
	  providers: [
			EventService,
			LocalStorageService,
			jsonObjectPipe,
			{provide: XHRBackend, useClass: MockBackend}
		]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEventFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*
  *	Test to verify component should be created
  */
  it('should be created', () => {
    expect(component).toBeTruthy();
  });
  
  //Testing request and response in success case.
  it('Test addEvent service', inject([EventService, XHRBackend], (EventService, mockBackend) => {
    //mock response to be expected from service
	const mockReponse = {
		status: 'success',
		responseData: {
			id: 1,
			name: 'event 01',
			description: 'Mock response description of event',
			latitude: '65.4321',
			longitude: '65.4321',
			start_date: '2017-08-30 03:33:00',
			end_date: '2017-09-06 15:33:00',
			rows: 5,
			columns: 3,
			poster: 'public/event_images/1Ht2GbVSy0Vu3wW08OsEZXUwVYhIk98jpRNGJY3o.jpeg',
				
		},
		error: ''
	};
	//mock request to pass valid data structure to the service for valid repsonse
	const mockRequest = {
		name: 'event 01',
		description: 'Mock response description of event',
		latitude: '65.4321',
		longitude: '65.4321',
		start_date: '2017-08-30 03:33:00',
		end_date: '2017-09-06 15:33:00',
		rows: 5,
		columns: 3,
		poster: 'public/event_images/1Ht2GbVSy0Vu3wW08OsEZXUwVYhIk98jpRNGJY3o.jpeg',
			
	}
	//mock request to pass valid file structure to the service for valid repsonse
	const mockFile = [
		{
			name: 'test.png',
			file: {				
				name: 'test.png',
				size: 2048,
				type: 'image/png'
			}
		}
	]
	//create mock response to be returned from service
	mockBackend.connections.subscribe((connection)=>{
		connection.mockRespond(new Response(new ResponseOptions({
			body: JSON.stringify(mockReponse)
		})));
	});
	//service call to verify correct reponse returned from serice as expected
	EventService.addEvent(mockRequest,mockFile).subscribe((result)=>{
		expect(result.status).toEqual('success');
		expect(result.responseData).not.toBeNull();
		expect(result.error).toEqual('');
	});
	
  }));
  
  //Testing request and response in error case.
  it('Test addEvent service', inject([EventService, XHRBackend], (EventService, mockBackend) => {
    //mock response to be expected from service
	const mockReponse = {
		status: 'error',
		responseData: '',
		error:{
			name:['The name field is required.'],
			descriptio:['The description field is required.'],
			start_date:['The start date field is required.'],
			end_date:['The end date field is required.'],
			columns:['The columns field is required.'],
			rows:['The rows field is required.'],
			latitude:['The latitude field is required.'],
			longitude:['The longitude format is invalid.']
		}
	};
	//mock request to pass valid data structure to the service for valid repsonse
	const mockRequest = {
		name: '',
		description: '',
		latitude: '',
		longitude: '',
		start_date: '',
		end_date: '',
		rows: null,
		columns: null			
	}
	//mock request to pass valid file structure to the service for valid repsonse
	const mockFile = [
		{
			name: '',
			file: {				
				name: '',
				size: 0,
				type: ''
			}
		}
	]
	//create mock response to be returned from service
	mockBackend.connections.subscribe((connection)=>{
		connection.mockRespond(new Response(new ResponseOptions({
			body: JSON.stringify(mockReponse)
		})));
	});
	//service call to verify correct reponse returned from serice as expected
	EventService.addEvent(mockRequest,mockFile).subscribe((result)=>{
		expect(result.status).toEqual('error');
		expect(result.error).not.toBeNull();
		expect(result.responseData).toEqual('');
	});
	
  }));
});
