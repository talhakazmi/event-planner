import { async, TestBed, ComponentFixture, inject } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpModule, Response, XHRBackend, ResponseOptions } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { HallComponent } from './hall.component';
import { numberLoopPipe } from '../../../../shared/pipes/NumberLoopingPipe';
import { StandComponent } from '../stand/stand.component';

import { EventService } from '../../event.service';

import { LocalStorageService, LocalStorageModule } from 'angular-2-local-storage';

describe('HallComponent', () => {
  let component: HallComponent;
  let fixture: ComponentFixture<HallComponent>;
  
  beforeEach(async(() => {
	//Configure testing module with components, services and modules required for testing
    TestBed.configureTestingModule({
      declarations: [ HallComponent, numberLoopPipe, StandComponent ],
	  providers: [ 
			numberLoopPipe, 
			EventService, 
			LocalStorageService,
			{ provide: XHRBackend, useClass: MockBackend }
		],
	  imports: [ 
			FormsModule,
			HttpModule,
			RouterTestingModule,
			LocalStorageModule.withConfig({
					prefix: "my-app",
					storageType: "localStorage"
				})
		]
    });
  }));
  
  beforeEach(() => {
    fixture = TestBed.createComponent(HallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*
  *	Test to verify component should be created
  */
  
  it('component should be  created', () => {
	 expect(component).toBeTruthy(); 
  });
  
  /*
  *	getDetail method in EventService get Event and stands detail from API.
  */
  it('getDetail Service test', inject([EventService, XHRBackend], (EventService, mockBackend) => {
    //mock response to be expected from service
	const mockResponse = {
				event: {
						id:4,
						name: 'event name',
						description: 'event description',
						latitude:42.0000,
						longitude:32.0000,
						start_date: '2017-08-30 03:33:00',
						end_date: '2017-09-06 15:33:00',
						rows:5,
						columns:5,
						poster:'public/event_images/1Ht2GbVSy0Vu3wW08OsEZXUwVYhIk98jpRNGJY3o.jpeg',
					},
				stands: [
						{
							id: 12,
							event_id: 4,
							cell_row: 0,
							cell_col: 0,
							company: 'company name',
							document: 'public/event_images/0OQIPlWy6AQStBPnZXt76wSX6L6mhhELBANZF8o6.doc',
							logo: 'public/event_images/P2ZrrgwFoOJCSWylGgXaEhFiJk2mnnu2Q6NI01wY.png',
							email: 'abc@company.com',
							price: 123,
							preview: 'public/event_images/OtHH1qiF2VNSV8uMEUWj6qAzr1a97ljDMCIuqfP6.jpeg',
							company_id: 9
							
						},
						{
							id: 13,
							event_id: 4,
							cell_row: 0,
							cell_col: 1,
							company: 'company name',
							document: 'public/event_images/0OQIPlWy6AQStBPnZXt76wSX6L6mhhELBANZF8o6.doc',
							logo: 'public/event_images/P2ZrrgwFoOJCSWylGgXaEhFiJk2mnnu2Q6NI01wY.png',
							email: 'abc@company.com',
							price: 123,
							preview: 'public/event_images/OtHH1qiF2VNSV8uMEUWj6qAzr1a97ljDMCIuqfP6.jpeg',
							company_id: 9
							
						}
					]
			};
	//create mock response to be returned from service  
	mockBackend.connections.subscribe((connection) => {
		  connection.mockRespond(new Response(new ResponseOptions({
			body: JSON.stringify(mockResponse)
		  })));
		});
	//service call to verify correct reponse returned from service as expected
	EventService.getDetail(9).subscribe((responseData) => {
		expect(responseData.event).not.toBeNull();
		expect(responseData.stands).not.toBeNull();
	});
	
  }));
});
