import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { EventService } from '../../event.service';
import {LocalStorageService} from 'angular-2-local-storage';

@Component({
  selector: 'app-hall',
  templateUrl: './hall.component.html',
  styleUrls: ['./hall.component.css']
})
export class HallComponent implements OnInit {
  
  id = null;
  event: Event;
  stands: any=[];
	
  constructor(
			//event service to call methods to communicate from API
			private eventService: EventService,
			//angular ActivatedRoute class for getting query string
			private route: ActivatedRoute,
			//local storage class to store data
			private localStorage: LocalStorageService
		) { }

/**
* on component initialize get id from query string via ActivatedRoute
* fetch event detail from API
**/
  ngOnInit() {
	  //Get id from url query string
	  this.route.params.subscribe((params) => this.id = params['id']);
	  //get event detail api call via event service getDetail method
	  this.eventService.getDetail(this.id).subscribe(
		//success or error returned from api.
		(response) => {
			this.event = response.responseData.event;
			for(let cell =0; cell<response.responseData.stands.length; cell++)
			{
				//stands list of events
				this.stands[response.responseData.stands[cell].cell_row+'-'+response.responseData.stands[cell].cell_col] = response.responseData.stands[cell];
			}
		},
		//error message from http
		(error) => {
			console.log(error);
		}
	  );
  }

}
/**
* Event interface to implement om map event fetch from DB via API
* public property define above implemeting Event interface
**/
interface Event {
	id:0;
	name:"";
	description:"";
	rows:0;
	columns:0;
	start_date:"";
	end_date:"";
	latitude:0.0;
	longitude:0.0;
	poster:"";
	created_at:"";
	updated_at:"";
}

