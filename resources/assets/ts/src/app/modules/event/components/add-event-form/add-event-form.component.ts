import { Component, Input } from '@angular/core';
//importing event services class
import { EventService } from '../../event.service';

@Component({
  selector: 'app-add-event-form',
  templateUrl: './add-event-form.component.html',
  styleUrls: ['./add-event-form.component.css']
})
export class AddEventFormComponent {
  
  //input property of component to get value from parent component via attribute location
  @Input() location: string;
  //public property to bind html inputs
  public event = {name:'', description:'', start_date:'', end_date:'', columns:'', rows:'', latitude:'', longitude:''};
  public success: string = '';
  //empty object for errors
  public error = {};
  
  //event service class to call methods to communicate from API
  constructor(private eventService: EventService) { }

  /**
  *	add event method calls on create event form submit
  * get event information from create event form, send information to api call to store event in DB
  **/
  addEvent() {
	  //store event image file name and image file in array of object files
	  let files = [{'name': (<HTMLInputElement>document.getElementById('inputFile')).name, 'file':(<HTMLInputElement>document.getElementById('inputFile')).files}]
	  //split location latitude and longitude by come to store separatly
	  let loc = this.location.split(",");
	  this.event.latitude = loc[0];
	  this.event.longitude = loc[1];
	  //add event api call via event service add event method
	  this.eventService.addEvent(this.event,files).subscribe(
	  (response)=>{
		  //success or error returned from api.
		  if(response.status == 'success')
			this.success = "Event saved successfuly.";
		  else
			this.error = response.error;
	  },
	  //error message from http
	  (error)=>{
		  this.error = error;
	  });
  }
  
}
