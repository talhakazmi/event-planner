import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { HttpModule, Response, XHRBackend, ResponseOptions } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { LocalStorageService, LocalStorageModule } from 'angular-2-local-storage';

import { ListStandComponent } from './list-stand.component';
import { EventService } from '../../event.service';

describe('ListStandComponent', () => {
  let component: ListStandComponent;
  let fixture: ComponentFixture<ListStandComponent>;
  
  //Configure testing module with components, services and modules required for testing
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListStandComponent ],
	  providers: 
		[ 
			EventService,
			LocalStorageService,
			{ provide: XHRBackend, useClass: MockBackend }
		],
	  imports: 
		[ 
			HttpModule,
			LocalStorageModule.withConfig({
				prefix: "my-app",
				storageType: "localStorage"
			})
		]
    })
    .compileComponents();
  }));
  
  beforeEach(() => {
    fixture = TestBed.createComponent(ListStandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  
  /*
  *	Test to verify component should be created
  */
  
  it('component should be  created', () => {
	 expect(component).toBeTruthy(); 
  });
  
  /*
  *	getStands method in EventService get stands list data from API.
  */
  
  //Testing request and response.
  it('should return an Observable<Array<stands>>',
	  inject([EventService, XHRBackend], (EventService, mockBackend) => {
		//mock response to be expected from service
		const mockResponse = [
			{ id: 0, event_id: 4, cell_row: 0, cell_col: 0, company: 'Company 0', document: 'document.doc', logo: 'logo.png', email: 'admin@company0.com' },
			{ id: 1, event_id: 4, cell_row: 0, cell_col: 1, company: 'Company 1', document: 'document.doc', logo: 'logo.png', email: 'admin@company0.com' },
			{ id: 2, event_id: 4, cell_row: 0, cell_col: 2, company: 'Company 2', document: 'document.doc', logo: 'logo.png', email: 'admin@company0.com' },
			{ id: 3, event_id: 4, cell_row: 0, cell_col: 3, company: 'Company 3', document: 'document.doc', logo: 'logo.png', email: 'admin@company0.com' },
		  ];
		//create mock response to be returned from service
		mockBackend.connections.subscribe((connection) => {
		  connection.mockRespond(new Response(new ResponseOptions({
			body: JSON.stringify(mockResponse)
		  })));
		});
		//service call to verify correct reponse returned from service as expected
		EventService.getStands(9).subscribe((stands) => {
		  expect(stands.length).not.toBeLessThan(0);
		  expect(stands).not.toBeNull();
		  expect(stands[2].company).toEqual('Company 2');
		  expect(stands[3].company).toEqual('Company 3');
		});
	}));
  
});

