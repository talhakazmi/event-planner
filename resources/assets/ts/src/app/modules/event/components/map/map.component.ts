import { Component, OnInit } from '@angular/core';
import { EventService } from '../../event.service';
import {LocalStorageService} from 'angular-2-local-storage';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  //default lat long to show map center
  lat: number = 51.678418;
  lng: number = 7.809007;
  //default zoom level to display map at zoom level 4
  zoom: number = 4;
  eventLatLng: string = '';
  
  totalMarkers: number = 0;
  //markers array of interface Marker
  markers: Marker[] = [];
  
  constructor(
			//event service to call methods to communicate from API
			private eventService: EventService,
			//local storage class to store data
			private localStorage: LocalStorageService
		) { }
  
  /**
  * on component initialize
  * fetch event list details via API and create markers to display on map
  **/
  ngOnInit() {
	  this.eventService.getList().subscribe(
	  (response)=>{
		  //events returned successfully add them in to markers array.
		  if(response.status == 'success')
			this.markers = response.responseData.events;
		this.totalMarkers = this.markers.length;
	  },
	  (error)=>{
		  console.log(error);
	  });
  }

  /**
  * map clicked method bind to output property of map click event
  * insert marker on clicked latlng and one at a time
  **/
  mapClicked(event) {
	  this.eventLatLng = event.coords.lat+','+event.coords.lng;
	  
	  if(this.markers.length > this.totalMarkers) {
		this.markers.pop();  
	  }
	  
	  this.markers.push({
		  latitude: event.coords.lat,
		  longitude: event.coords.lng,
		  name: 'D',
		  description: 'New Marker Added',
		  draggable: false
		});
  }
  
  updateMarker(m, event) {
	console.log(this.markers);
  }
}
/**
* Marker interface to impelement on marker array to map properties corrrectly.
**/
interface Marker {
	latitude: number;
	longitude: number;
	name?: string;
	description?: string;
	draggable: boolean;
}