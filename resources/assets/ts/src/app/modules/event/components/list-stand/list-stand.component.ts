import { Component, OnInit } from '@angular/core';
import { EventService } from '../../event.service';
import {LocalStorageService} from 'angular-2-local-storage';

@Component({
  selector: 'app-list-stand',
  templateUrl: './list-stand.component.html',
  styleUrls: ['./list-stand.component.css']
})
export class ListStandComponent implements OnInit {

  stands=[];
  error: string = '';

  constructor(
			//event service to call methods to communicate from API
			private eventService: EventService,
			//local storage class to store data
			private localStorage: LocalStorageService
		) { }

  /**
  * on component initialize
  * fetch stands list details via company/user id from local storage
  **/
  ngOnInit() {
	  //get stands booked by user/company id logged in via local storage
	  this.eventService.getStands(this.localStorage.get('id')).subscribe(
	  //success or error returned from api.
	  (response)=>{
		  //if stands exist show list else print error
		  if(response.length > 0)
			this.stands = response;
		  else
			this.error = 'You haven\'t booked any stand yet';
	  },
	  //error message from http
	  (error)=>{
		  this.error = error;
	  });
  }

}
