import { Component, OnInit } from '@angular/core';
import { EventService } from '../../event.service';

@Component({
  selector: 'app-list-event',
  templateUrl: './list-event.component.html',
  styleUrls: ['./list-event.component.css']
})
export class ListEventComponent implements OnInit {

  events: any = [];
  error: string = '';
	
  //event service class to call methods to communicate from API
  constructor(private eventService: EventService) { }

  /**
  * on component initialize
  * fetch event list from API
  **/
  ngOnInit() {
	  //get event list with stands api call via event service getList method
	  this.eventService.getList().subscribe(
	  //success or error returned from api.
	  (response)=>{
		  if(response.status == 'success')
			this.events = response.responseData.events;
		  else
			this.error = response.error;
	  },
	  //error message from http
	  (error)=>{
		  this.error = error;
	  });
  }

}
