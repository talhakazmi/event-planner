import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { EventService } from '../../event.service';
import {LocalStorageService} from 'angular-2-local-storage';

@Component({
  selector: 'app-stand-detail',
  templateUrl: './stand-detail.component.html',
  styleUrls: ['./stand-detail.component.css']
})
export class StandDetailComponent implements OnInit {

  id: number = 0;
  stand: Stand;
  error: string = '';
  
  constructor(
			//event service to call methods to communicate from API
			private eventService: EventService,
			//angular ActivatedRoute class for getting query string
			private route: ActivatedRoute,
			//angular router class for navigation
			private router: Router,
			//local storage class to store data
			private localStorage: LocalStorageService
		) {}
		
  /**
  * on component initialize get id from query string via ActivatedRoute
  * fetch stands list details via API 
  **/
  ngOnInit() {
	  this.route.params.subscribe((params) => this.id = params['id']);
	  this.eventService.getStand(this.id).subscribe(
		(response) => {
			this.stand = response;
			console.log(this.stand);
		},
		(error) => {
			this.error = error;
		}
	  );
  }
  /**
  * Book stand to store stand with company/user id
  **/
  bookStand() {
	  //get company/user id from localstorage of logged in user/company
	  this.stand.company_id = JSON.parse(this.localStorage.get('id').toString());
	  //store stand image file name and image file in array of object files with logo file and file name
	  let files = [
			{'name': (<HTMLInputElement>document.getElementById('document-input')).name, 'file': (<HTMLInputElement>document.getElementById('document-input')).files},
			{'name': (<HTMLInputElement>document.getElementById('logo-input')).name, 'file': (<HTMLInputElement>document.getElementById('logo-input')).files},
		];
	  //POST service with files and company id and stand id to book
	  this.eventService.bookStand(this.id,this.stand,files).subscribe(
	  (response)=>{
		  if(response.status == 'success')
			this.router.navigate(['event/'+this.stand.event_id]);
		  else
			this.error = response.error;
	  },
	  (error)=>{
		  this.error = error;
	  });
  }

}
/**
* Stand interface to impelement on stand array to map properties corrrectly.
**/
interface Stand {
	id:3
	cell_col:0,
	cell_row:0,
	company:null,
	document:null,
	logo:null,
	price:null,
	event_id:null,
	company_id:null,
	email:null,
	created_at:"",
	updated_at:""
}