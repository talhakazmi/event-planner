import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { AgmCoreModule } from '@agm/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, Response, XHRBackend, ResponseOptions } from '@angular/http';
import { MockBackend } from '@angular/http/testing';

import { MapComponent } from './map.component';
import { AddEventFormComponent } from '../add-event-form/add-event-form.component';
import { ListStandComponent } from '../list-stand/list-stand.component';
import { ListEventComponent } from '../list-event/list-event.component';

import { jsonObjectPipe } from '../../../../shared/pipes/ObjectLoopingPipe';

import { EventService } from '../../event.service';
import { LocalStorageService, LocalStorageModule } from 'angular-2-local-storage';

describe('MapComponent', () => {
  let component: MapComponent;
  let fixture: ComponentFixture<MapComponent>;

  //Configure testing module with components, services and modules required for testing
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapComponent, AddEventFormComponent, ListStandComponent, ListEventComponent, jsonObjectPipe ],
	  imports: [ 
			AgmCoreModule.forRoot({
				apiKey: 'AIzaSyAZMtrLIJH2iA0k2rGaST9X99CUEAK_8_8'
			}),
			FormsModule,
			HttpModule,
			LocalStorageModule.withConfig({
					prefix: "my-app",
					storageType: "localStorage"
				})
		],
	  providers: [
			EventService,
			jsonObjectPipe,
			LocalStorageService,
			{provide: XHRBackend, useClass: MockBackend}
		]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*
  *	Test to verify component should be created
  */
  
  it('should be created', () => {
    expect(component).toBeTruthy();
  });
  
  /*
  *	getList method in EventService get list of events from API.
  */
  
  //Testing request and response in success case.
  
  it('Test list event service', inject([EventService, XHRBackend], (EventService, mockBackend) => {
	
	//mock response to be expected from service
	const mockResponse = {
		status: 'success',
		responseData: {
			events: [
				{				
					id: 1,
					name: 'event 01',
					description: 'Mock response description of event',
					latitude: '65.4321',
					longitude: '65.4321',
					start_date: '2017-08-30 03:33:00',
					end_date: '2017-09-06 15:33:00',
					rows: 5,
					columns: 3,
					poster: 'public/event_images/1Ht2GbVSy0Vu3wW08OsEZXUwVYhIk98jpRNGJY3o.jpeg',
				},
				{				
					id: 2,
					name: 'event 02',
					description: 'Mock response description of event',
					latitude: '65.4321',
					longitude: '65.4321',
					start_date: '2017-08-30 03:33:00',
					end_date: '2017-09-06 15:33:00',
					rows: 5,
					columns: 3,
					poster: 'public/event_images/1Ht2GbVSy0Vu3wW08OsEZXUwVYhIk98jpRNGJY3o.jpeg',
				}
			]
		},
		error: ''
	};
	//create mock response to be returned from service
	mockBackend.connections.subscribe((connection)=>{
		connection.mockRespond( new Response( new ResponseOptions({
			body: JSON.stringify(mockResponse)
		})));
	});
	
	//service call to verify correct reponse returned from serice as expected
	EventService.getList().subscribe((result)=>{
		expect(result).not.toBeNull();
		expect(result.status).toEqual('success');
		expect(result.responseData).not.toBeNull();
		expect(result.responseData.events).not.toBeNull();
		expect(result.error).toEqual('');
	});
	
  }))
});
