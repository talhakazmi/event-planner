import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpModule, Response, XHRBackend, ResponseOptions } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { VisitorFormComponent } from './visitor-form.component';
import { jsonObjectPipe } from '../../../../shared/pipes/ObjectLoopingPipe';

import { EventService } from '../../event.service';

import { LocalStorageService, LocalStorageModule } from 'angular-2-local-storage';

describe('VisitorFormComponent', () => {
  let component: VisitorFormComponent;
  let fixture: ComponentFixture<VisitorFormComponent>;
  //Configure testing module with components, services and modules required for testing
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisitorFormComponent, jsonObjectPipe ],
      providers: [
			jsonObjectPipe,
			EventService,
			LocalStorageService,
			{provide: XHRBackend, useClass: MockBackend}
		],
	  imports: [ 
			FormsModule,
			HttpModule,
			RouterTestingModule,
			LocalStorageModule.withConfig({
					prefix: "my-app",
					storageType: "localStorage"
				})
		]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitorFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  
  /*
  *	getVisitors method in EventService get visitors data to the stand via stand id.
  */
  it('should be created', inject([EventService, XHRBackend], (EventService, mockBackend) => {
    //mock response to be expected from service
	const mockResponse = [
						{
							id: 1,
							stand_id: 13,
							name: 'visitor 01',
							email: 'abc@company.com',
							occupation: 123
							
						},
						{
							id: 2,
							stand_id: 13,
							name: 'visitor 02',
							email: 'abc@company.com',
							occupation: 123
							
						}
					];
	//create mock response to be returned from service			
	mockBackend.connections.subscribe((connection) => {
		connection.mockRespond( new Response( new ResponseOptions({
			body: JSON.stringify(mockResponse)
		})));
	});
	//service call to verify correct reponse returned from service as expected
	EventService.getVisitors(13).subscribe((responseData) => {
		expect(responseData).not.toBeNull();
		expect(responseData.length).toBeGreaterThan(0);
	});
	
	expect(component).toBeTruthy();
  }));
});
