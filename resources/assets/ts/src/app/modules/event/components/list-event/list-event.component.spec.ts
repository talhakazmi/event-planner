import { async, inject, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpModule, Response, ResponseOptions, XHRBackend } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { LocalStorageService, LocalStorageModule } from 'angular-2-local-storage';

import { ListEventComponent } from './list-event.component';
import { EventService } from '../../event.service';

describe('ListEventComponent', () => {
  let component: ListEventComponent;
  let fixture: ComponentFixture<ListEventComponent>;

  //Configure testing module with components, services and modules required for testing
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListEventComponent ],
      providers: [
			EventService,
			LocalStorageService,
			{provide: XHRBackend, useClass: MockBackend}
		],
	  imports: 
		[ 
			HttpModule,
			LocalStorageModule.withConfig({
				prefix: "my-app",
				storageType: "localStorage"
			})
		]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*
  *	Test to verify component should be created
  */
  it('should be created', () => {
    expect(component).toBeTruthy();
  });
  
  /*
  *	getList method in EventService get list of events from API.
  */
  
  //Testing request and response in success case.
  
  it('Test list event service', inject([EventService, XHRBackend], (EventService, mockBackend) => {
	
	//mock response to be expected from service
	const mockResponse = {
		status: 'success',
		responseData: {
			events: [
				{				
					id: 1,
					name: 'event 01',
					description: 'Mock response description of event',
					latitude: '65.4321',
					longitude: '65.4321',
					start_date: '2017-08-30 03:33:00',
					end_date: '2017-09-06 15:33:00',
					rows: 5,
					columns: 3,
					poster: 'public/event_images/1Ht2GbVSy0Vu3wW08OsEZXUwVYhIk98jpRNGJY3o.jpeg',
				},
				{				
					id: 2,
					name: 'event 02',
					description: 'Mock response description of event',
					latitude: '65.4321',
					longitude: '65.4321',
					start_date: '2017-08-30 03:33:00',
					end_date: '2017-09-06 15:33:00',
					rows: 5,
					columns: 3,
					poster: 'public/event_images/1Ht2GbVSy0Vu3wW08OsEZXUwVYhIk98jpRNGJY3o.jpeg',
				}
			]
		},
		error: ''
	};
	//create mock response to be returned from service
	mockBackend.connections.subscribe((connection)=>{
		connection.mockRespond( new Response( new ResponseOptions({
			body: JSON.stringify(mockResponse)
		})));
	});
	
	//service call to verify correct reponse returned from serice as expected
	EventService.getList().subscribe((result)=>{
		expect(result).not.toBeNull();
		expect(result.status).toEqual('success');
		expect(result.responseData).not.toBeNull();
		expect(result.responseData.events).not.toBeNull();
		expect(result.error).toEqual('');
	});
	
  }))
});
