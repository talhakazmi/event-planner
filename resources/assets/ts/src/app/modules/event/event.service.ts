import { Injectable } from '@angular/core';
import { Http} from '@angular/http';
import { RestClientService } from '../../services/rest-client.service';
import { LocalStorageService } from 'angular-2-local-storage';

@Injectable()
export class EventService extends RestClientService {

  //Event API urls for event activities
  GET_EVENT_LIST = "event/list";
  EVENT_DETAIL = "event/";
  ADD_EVENT = "event/add";
  ADD_STAND = "stand/add";
  STAND_DETAIL = "stand/";
  GET_STAND_LIST = "stands/";
  STAND_UPDATE = "stand/";
  ADD_VISITOR = "visitor";
  EMAIL_LOG = "email/";
  GET_VISITOR = "visitor/";
  
  //passing http and localstorage property to rest client class
  constructor(http: Http, localStorage: LocalStorageService) {
    super(http, localStorage);
  }
  /**
  *	getList method return all upcoming or running events list
  *	Return:
  *		{status: '', responseData: {events: []}, error: ''}
  **/
  public getList(){
    return this.get(this.GET_EVENT_LIST);
  }
  
  /**
  *	getDetail method return event detail with its stands on the basis of event id
  * Params:
  *		id: number
  *	Return:
  *		{status: '', responseData: {event: {}, stands: []}, error: ''}
  **/
  public getDetail(id){
    return this.get(this.EVENT_DETAIL + id);
  }
  
  /**
  *	addEvent method add event in data base via API returns added event detail.
  * Params:
  *		event: Event
  *		files: [File]
  *	Return:
  *		{status: '', responseData: {event: {}}, error: ''}
  **/
  public addEvent(params,file) {
    return this.postFormData(this.ADD_EVENT,params,file);
  }
  
  /**
  *	addStand method add stand detail in the data base via API
  * Params:
  *		stand: Stand
  *		files: [File, File]
  *	Return:
  *		{status: '', responseData: {stand: {}}, error: ''}
  **/
  public addStand(params,file) {
    return this.postFormData(this.ADD_STAND,params,file);
  }
  
  /**
  *	getStand method return stand data on the basis of stand id
  * Params:
  *		id: number
  *	Return:
  *		{status: '', responseData: {stand: {}}, error: ''}
  **/
  public getStand(id){
    return this.get(this.STAND_DETAIL + id);
  }
  
  /**
  *	getStand method update stand detail with company specific detail and return newly added stand data
  * Params:
  *		{'company': '', 'email': '', 'document': '', 'logo': '', 'company_id': ''}
  *	Return:
  *		{status: '', responseData: {stand: {}}, error: ''}
  **/
  public bookStand(id,params,file) {
    return this.postFormData(this.STAND_UPDATE+id,params,file);
  }
  
  /**
  *	getStands method return stands list on the basis of company id (company's booked stands)
  * Params:
  *		id: number (logged in company id)
  *	Return:
  *		[stand, stand, ... ]
  **/
  public getStands(id){
    return this.get(this.GET_STAND_LIST + id);
  }
  
  /**
  *	addVisitor method add visitors data in data base against stand
  * Params:
  *		{'stand_id': '', 'name': '', 'email': '', 'occupation': ''}
  *	Return:
  *		{status: '', responseData: {visitor: {}}, error: ''}
  **/
  public addVisitor(params){
    return this.post(this.ADD_VISITOR, params);
  }
  
  /**
  *	emailLog method sends visitors list/log in email to stand admin/company email
  * Params:
  *		id: number (stand id)
  **/
  public emailLog(id){
    return this.get(this.EMAIL_LOG + id);
  }
  
  /**
  *	getVisitors method return visitors list on the basis of stand id
  * Params:
  *		id: number (stand id)
  *	Return:
  *		[visitor, visitor, ... ]
  **/
  public getVisitors(id){
    return this.get(this.GET_VISITOR + id);
  }
}
