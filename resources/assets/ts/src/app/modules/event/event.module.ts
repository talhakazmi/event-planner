import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { CommonModule } from '@angular/common';
import { EventRoutingModule } from './event-routing.module';
import { AgmCoreModule } from '@agm/core';
//importing event related components in module
import { MapComponent } from './components/map/map.component';
import { HallComponent } from './components/hall/hall.component';
import { StandComponent } from './components/stand/stand.component';
import { AddEventFormComponent } from './components/add-event-form/add-event-form.component';
import { ListEventComponent } from './components/list-event/list-event.component';
import { StandDetailComponent } from './components/stand-detail/stand-detail.component';
import { ListStandComponent } from './components/list-stand/list-stand.component';
import { VisitorFormComponent } from './components/visitor-form/visitor-form.component';
//importing event services class
import { EventService } from './event.service';
//importing shared module to load all shared pipes etc in event
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  //loading modules in event module
  imports: [
    CommonModule,
	FormsModule,
	EventRoutingModule,
	SharedModule,
	//Angular Google Map (agm) module loaded with google map api key in configuration
	AgmCoreModule.forRoot({
		apiKey: 'AIzaSyAZMtrLIJH2iA0k2rGaST9X99CUEAK_8_8'
	})
  ],
  //loading components in event module
  declarations: [
	MapComponent, 
	HallComponent, 
	StandComponent, 
	AddEventFormComponent, 
	ListEventComponent,
	StandDetailComponent,
	ListStandComponent,
	VisitorFormComponent
  ],
  //module providing event service
  providers: [
	EventService
  ]
})
export class EventModule { }
