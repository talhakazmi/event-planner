import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
//importing components to be used in routing
import { MapComponent } from './components/map/map.component';
import { HallComponent } from './components/hall/hall.component';
import { StandDetailComponent } from './components/stand-detail/stand-detail.component';
import { VisitorFormComponent } from './components/visitor-form/visitor-form.component';
//Auth guard to prevent user navigation on pages not allowed for not logged in user
import { AuthGuard } from '../../services/auth-guard.service';
//defining routes for user module.
const routes: Routes = [
  {
	//route match to event
    path: 'event',
	//children paths to map and hall components
    children:[
	  //canActivate activate route on condition written in AuthGuard class method canActivate
      { path: 'map', component: MapComponent, canActivate: [AuthGuard] },
      { path: ':id', component: HallComponent, canActivate: [AuthGuard] }
    ]
  },
  {
	  //route match to stand
	  path: 'stand',
	  //children paths to stand components
	  children: [
	  //canActivate activate route on condition written in AuthGuard class method canActivate
		{ path: ':id', component: StandDetailComponent, canActivate: [AuthGuard] }
	  ]
  },
  {
	  //route match to visitor
	  path: 'visitor',
	  //children paths to visitor components
	  children: [
		//canActivate activate route on condition written in AuthGuard class method canActivate
		{ path: ':id', component: VisitorFormComponent, canActivate: [AuthGuard] }
	  ]
  }
  
  
];

@NgModule({
  //importing given route
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class EventRoutingModule { }
