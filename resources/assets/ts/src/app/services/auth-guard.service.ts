import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import {LocalStorageService} from 'angular-2-local-storage';

@Injectable() //emit the needed metadata
//CanActivate is a guard to decide if a route can be activated
export class AuthGuard implements CanActivate {

  constructor(private localStorage: LocalStorageService, private router: Router) {}
  /**
  * method called from routing-module
  * canActivate method checks if token not exist navigate user to home screen.
  */
  canActivate() {
    if(this.localStorage.get('token') === null) {
	  
	  this.router.navigate(['/home']);
      return false;
    } else {
	  return true;
    }
  }
}