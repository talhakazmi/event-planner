import { Injectable } from '@angular/core';

import {Headers, Http, RequestOptions, Response} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { LocalStorageService } from 'angular-2-local-storage';

//emit the needed metadata
@Injectable()
export class RestClientService {

  private headers: any;
  private formDataHeaders: any;
  private SERVICES_URL : string = "http://localhost/my-app/project/public/api/";
  
  constructor(
		protected http: Http,
		private localStorage: LocalStorageService
	) {
	  //Create headers to be post in all services with token
	  this.headers = new Headers({'Content-Type': 'application/json', 'Authorization': 'Bearer '+this.localStorage.get('token')});
	  this.formDataHeaders = new Headers({'Authorization': 'Bearer '+this.localStorage.get('token')});
	}

  /**
  * params: url (API url to be called)
  * get method return observable response
  */
  public get(url): Observable<any> {
	//Creates a request options object with header
    let options = new RequestOptions({ headers: this.headers });
    return this.http.get(this.SERVICES_URL + url,options)
				//extractData private method to return json of response
               .map(this.extractData)
				//handleError private method to return Observable response of error returned from http
               .catch(this.handleError);
  }
  /**
  * params: 
  *		url (API url to be called)
  *		params (POST params required by API)
  * post method return observable response
  */
  public post(url,params): Observable<any> {
	//Creates a request options object with header
    let options = new RequestOptions({ headers: this.headers });
    return this.http.post(this.SERVICES_URL + url, params , options)
                    //extractData private method to return json of response
					.map(this.extractData)
                    .catch(this.handleError);
  }
  
  /**
  * params: 
  *		url (API url to be called)
  *		params (POST params required by API)
  *		files (uploaded file to be post in formData)
  * postFormData method return observable response
  */
  public postFormData(url,params,files): Observable<any> {
    //Creates a request options object with header
	let options = new RequestOptions({ headers: this.formDataHeaders });
    //create formData variable of type FormData	
	let formData: FormData = new FormData();
	//Append formData with files of type File
	for(let count = 0; count<files.length; count++) {
		//access the list of files selected with input type file
		let fileList: FileList = files[count].file;
		if(fileList.length > 0) {
			//File Object from fileList object return 
			let file: File = fileList[0];
			//Append file in formData with file name
			formData.append(files[count].name, file, file.name);
		}
	}
	//Append params other than files in formData
	for (let key in params) {
		formData.append(key, params[key]);
	}
	///return observable
	return this.http.post(this.SERVICES_URL + url, formData, options)
		//extractData private method to return json of response
		.map(this.extractData)
		.catch(this.handleError);
  }
  //return response json 
  private extractData(res: Response) {
    return res.json();
  }
  //return observable of error form http
  private handleError (error: Response | any) {
    return Observable.throw(error);
  }
}

