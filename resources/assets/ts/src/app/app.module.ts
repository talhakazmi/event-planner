import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, Router } from '@angular/router';
import { HttpModule } from '@angular/http';
//Third Party local storage module
import { LocalStorageModule } from 'angular-2-local-storage';
//Jquery Library to manipulate html simply.
import * as $ from 'jquery';

import { AppComponent } from './app.component';
//Rest Client for entertaining API from server from a centralized class
import { RestClientService } from './services/rest-client.service';
//AuthGuard to handle user authorization across site
import { AuthGuard } from './services/auth-guard.service';

//App routing module to define route for app component
import { AppRoutingModule } from './app-routing.module';
//All modules to include in app module
import { UserModule } from './modules/user/user.module';
import { PublicModule } from './modules/public/public.module';
import { EventModule } from './modules/event/event.module';
import { SharedModule } from './shared/shared.module';



@NgModule({
  declarations: [
    AppComponent,
  ],
  //Importing modules to be used with in application
  imports: [
    HttpModule,
    BrowserModule,
	PublicModule,
	UserModule,
	EventModule,
	SharedModule,
	AppRoutingModule,
	//local storage module configuration
	LocalStorageModule.withConfig({
		prefix: "my-app",
		storageType: "localStorage"
	})
  ],
  //providing services for rest client and authorization
  providers: [
	RestClientService,
	AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
