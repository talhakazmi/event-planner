import { Component } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // public property for displaying application title
  title: string = 'app';
  //localstorage is public for use in template 
  constructor(public localStorage: LocalStorageService) {  }
  
}
