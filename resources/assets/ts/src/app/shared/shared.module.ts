import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//import shared pipes in shared module
import { jsonObjectPipe } from './pipes/ObjectLoopingPipe';
import { numberLoopPipe } from './pipes/NumberLoopingPipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
	jsonObjectPipe,
	numberLoopPipe
  ],
  exports:[
	jsonObjectPipe,
	numberLoopPipe
  ],
  providers: [
	jsonObjectPipe,
	numberLoopPipe
  ]
})
export class SharedModule { }
