import { Pipe, PipeTransform, Inject} from '@angular/core';
//creating pipe for looping on Object rather than array
@Pipe({name: 'jsonObject'})
export class jsonObjectPipe implements PipeTransform {
  //get object to be looped and return an array of objects created using the given json object
  transform(value, args:string[]) : any {
    let keys = [];
    for (let key in value) {
      keys.push({key: key, value: value[key]});
    }
    return keys;
  }
}