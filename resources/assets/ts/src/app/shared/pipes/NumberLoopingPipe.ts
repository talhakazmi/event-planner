import { Pipe, PipeTransform, Inject} from '@angular/core';
//creating custom pipe "numberLoop" for looping over fixed number of times
@Pipe({name: 'numberLoop'})
export class numberLoopPipe implements PipeTransform {
  transform(value) {
    //return array of length value
	return (new Array(value)).fill(1);
  }
}