<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Crossover - project</title>
<style type="text/css">
body{
	font-family:Arial, Helvetica, sans-serif;
	font-size:13px;
	color:#333333;
}
a{
	color:#0088cc;
	text-decoration:none;
}
a:hover{
	color:#1c1c1c;
}
h1{
	font-size:25px;
	font-weight:bold;
}
p{
	font-family:Arial, Helvetica, sans-serif;
	font-size:13px;
}

h3{
	font-size:16px;
	font-weight:bold;
}
</style>
</head>

<body style="background-color:#eeeeee;">

<table cellpadding="0" cellspacing="0" style="width:100%;">
	
    <tr>
    	<td align="center">        
        	<table cellpadding="0" cellspacing="0" border="0" style="width:600px;">
            	
                <tr>
                	<td style="background-color:#A6ACE4;"><img alt="logo" src="https://app.crossover.com/x/images/3d30c71a.logo.svg" /></td>
                </tr>
                <!-- Body Content -->
                <tr>
                	<td style="background-color:#fff; padding:10px 50px;">
                    <p>Hey <strong>Admin</strong>,</p>
                    <p>
						Kindly receive the list of visitors on you stand during the event.
					</p>
					</td>
                </tr>
				<tr>
					<table border="1">
						<tr>
							<td>Name</td>
							<td>Email</td>
							<td>Occupation</td>
						</tr>
						<?php foreach($visitors as $visitor): ?>
						<tr>
							<td><?php echo $visitor->name ?></td>
							<td><?php echo $visitor->email ?></td>
							<td><?php echo $visitor->occupation ?></td>
						</tr>
						<?php endforeach; ?>
					</table>
				</tr>
                <!-- end Body Content -->
                
                <!-- footer section -->
                <tr>
                	<td style="background-color:#A6ACE4; padding:10px 20px; height:100px; color:#fff; vertical-align:top;">
                    	
                        <table>
                        	<tr>
                        		<td align="center" style="height:60px; padding:0px 148px; color:#666; text-align:center;">Copyright &copy;</td>
                        	</tr>
                        </table>
                       
                    </td>
                </tr>
                <!-- end footer section -->               
            </table>
        </td>
    </tr>
    
</table>
</body>
</html>
