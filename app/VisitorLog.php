<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitorLog extends Model
{
    //table name varies from convention.
	protected $table = 'visitor_log';
}
