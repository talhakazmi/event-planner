<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class EventController extends Controller
{
    /**
     * Show the event detail for the given id.
     *
     * @param  int  $id
     * @return Response
     */
    public function __invoke($id)
    {
		return response()->json(['status'=>'success','responseData'=>['event'=>Event::findOrFail($id)], 'error'=>'']);
    }
	
	/**
	* add function call on add event API
	* type: POST
	* param: $request $_POST and $_FILES
	* response success if event detail added else error
	**/
	public function add(Request $request)
	{
		// grab event detail from the request
        $input = $request->only('name', 'description', 'start_date', 'end_date', 'columns', 'rows', 'poster', 'latitude', 'longitude');
		//apply validation rules on request
		$rules = [
                'name' => 'required|string|max:100',
                'description' => 'required|string|max:255',
                'start_date' => 'required|date_format:Y-m-d\TH:i|after:today',
				'end_date' => 'required|date_format:Y-m-d\TH:i|after:start_date',
				'columns' => 'required|numeric|min:1',
				'rows' => 'required|numeric|min:1',
				'latitude' => 'required|regex:/^[+-]?\d+\.\d+/',
				'longitude' => 'required|regex:/^[+-]?\d+\.\d+/',
				'poster' => 'required'
                ];
		
		$validate = Validator::make($input,$rules);
		//check validation passed or failed and event poster given is valid
		if(!$validate->fails() && $request->file('poster')->isValid())
        {
            //store poster on folder and get path of file.
			$path = $request->poster->store('public/event_images');
			
			$event = new Event;

            $event->name = $request->name;
            $event->description = $request->description;
            $event->start_date = $request->start_date;
            $event->end_date = $request->end_date;
            $event->columns = $request->columns;
            $event->rows = $request->rows;
            $event->latitude = $request->latitude;
            $event->longitude = $request->longitude;
            $event->poster = $path;

            $event->save();

            return response()->json(['status'=>'success','responseData'=>['event'=>$event], 'error'=>'']);
        }
        else
        {
            return response()->json(['status'=>'failed','responseData'=>'', 'error'=>$validate->errors()]);
        }
	}
	
	/**
	* upcomingEvents function call on event list to display on home page
	* type: GET
	* response success if event list exist else error
	**/
	public function upcomingEvents()
	{
		//make query to get event with start date less than current date with order by desc
		$event = Event::where('start_date', '>=', 'SYSDATE()')
               ->orderBy('name', 'desc')
               ->get();
		if($event)
		{
			return response()->json(['status'=>'success','responseData'=>['events'=>$event], 'error'=>'']);
		} else {
			return response()->json(['status'=>'failed','responseData'=>'', 'error'=>'No Event found']);
		}
	}
	
	/**
	* get function call on event detail API
	* type: GET
	* param: event id
	* response success if event exist else error
	**/
	public function get($id)
	{
		$event = Event::find($id);
		if($event)
		{
			return response()->json(['status'=>'success','responseData'=>['event'=>$event, 'stands'=>$event->stands], 'error'=>'']);
		} else {
			return response()->json(['status'=>'failed','responseData'=>'', 'error'=>'No Event found']);
		}
	}
}