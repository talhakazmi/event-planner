<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;


class RegisterController extends Controller
{
	/**
	* sign up function call on add user API
	* type: POST
	* param: $request $_POST
	* response success if user added else error
	**/
    public function signup(Request $request){
        //apply validation rules on request
		$rules = [
                'email' => 'required|email|unique:users',
                'name' => 'required|string|min:6',
                'password' => 'required|min:6|confirmed',
				'password_confirmation' => 'required|min:6'
                ];
        $field = ['name','email','password','password_confirmation'];
        $input = $request->only($field);
        $validate = Validator::make($input,$rules);
		//check validation passed or failed
        if(!$validate->fails())
        {
            $user = new User;

            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = bcrypt($request->input('password'));
			//If user is admin make is admin bit true
            if($request->input('is_admin'))
            {
                $user->is_admin = 1;
            }

            $user->save();
			//return user detail along with token to be stored and logged in user
            return response()->json(['status'=>'success','responseData'=>['token'=>JWTAuth::fromUser($user),'user'=>$user], 'error'=>'']);
        }
        else
        {
            return response()->json(['status'=>'failed','responseData'=>'', 'error'=>$validate->errors()]);
        }
    }
}
