<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;


class LoginController extends Controller
{
    public function signin(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['status'=>'failed','responseData'=> '', 'error' => ['email'=>['Invalid Credentials']]]);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['status'=>'failed','responseData'=>'', 'error' => ['email'=>['could_not_create_token']]]);
        }

        $user   =   User::where('email',$request->input('email'))->first();

        $token = compact('token');
        // all good so return the token
        return response()->json(['status'=>'success','responseData'=>['token'=>$token['token'],'user'=>$user], 'error'=>'']);
    }
}
