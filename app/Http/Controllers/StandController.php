<?php

namespace App\Http\Controllers;

use Mail;
use App\User;
use App\Stand;
use App\VisitorLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StandController extends Controller
{
	/**
	* add function call on add stand API
	* type: POST
	* param: $request $_POST and $_FILES
	* response success if stand detail added else error
	**/
    public function add(Request $request)
	{
		// grab stand detail from the request
        $input = $request->only('price','event_id','cell_row','cell_col','preview');
		//apply validation rules on request
		$rules = [
                'price' => 'required|numeric',
				'event_id' => 'required|numeric',
				'cell_row' => 'required|numeric',
				'cell_col' => 'required|numeric',
				'preview' => 'required|mimes:jpeg,jpg,png|max:2048'
                ];
		
		$validate = Validator::make($input,$rules);
		//check validation passed or failed
		if(!$validate->fails())
        {
            //store stand preview image in folder
			$path = $request->preview->store('public/event_images');
			
			$stand = new Stand;
			//map stand information
            $stand->price = $request->price;
            $stand->event_id = $request->event_id;
            $stand->cell_row = $request->cell_row;
            $stand->cell_col = $request->cell_col;
            $stand->preview = $path;
			//save stand detail
            $stand->save();

            return response()->json(['status'=>'success','responseData'=>['stand'=>$stand], 'error'=>'']);
        }
        else
        {
            return response()->json(['status'=>'failed','responseData'=>'', 'error'=>$validate->errors()]);
        }
	}
	
	/**
	* update function call on book stand API
	* type: POST
	* param: $request $_POST and $_FILES
	* response success if stand booked or bind with compabny id else error
	**/
	public function update(Request $request,$id)
	{
		//grab stand detail from the request
		$input = $request->only('company', 'email', 'document', 'logo', 'company_id');
		//apply validation rules on request
		$rules = [
                'company' => 'required|string|max:250',
                'email' => 'required|email',
				'document' => 'required|mimes:doc,docx,pdf|max:1024',
				'logo' => 'required|mimes:jpeg,jpg,png|max:2048',
				'company_id' => 'required|numeric'
                ];
		
		$validate = Validator::make($input,$rules);
		//check validation passed or failed
		if(!$validate->fails())
        {
            //store stand preview image in folder
			$docPath = $request->document->store('public/event_images');
            $logoPath = $request->logo->store('public/event_images');
			
			$stand = Stand::find($id);

            $stand->company = $request->company;
            $stand->email = $request->email;
            $stand->document = $docPath;
            $stand->logo = $logoPath;
            $stand->company_id = $request->company_id;

            $stand->save();

            return response()->json(['status'=>'success','responseData'=>['stand'=>$stand], 'error'=>'']);
        }
        else
        {
            return response()->json(['status'=>'failed','responseData'=>'', 'error'=>$validate->errors()]);
        }
	}
	
	/**
	* add_visitor function call on add visitor to stand API
	* type: POST
	* param: $request $_POST and $_FILES
	* response success if visitor added with stand id else error
	**/
	public function add_visitor(Request $request)
	{
		//grab stand detail from the request
		$input = $request->only('stand_id','name','email','occupation');
		//apply validation rules on request
		$rules = [
                'stand_id' => 'required|numeric',
				'name' => 'required|string',
				'email' => 'required|email|unique:visitor_log',
				'occupation' => 'required|string'
                ];
		
		$validate = Validator::make($input,$rules);
		//check validation passed or failed
		if(!$validate->fails())
        {
            $visitor = new VisitorLog;

            $visitor->stand_id= $request->stand_id;
            $visitor->name= $request->name;
            $visitor->email= $request->email;
            $visitor->occupation= $request->occupation;

            $visitor->save();

            return response()->json(['status'=>'success','responseData'=>['visitor'=>$visitor], 'error'=>'']);
        }
        else
        {
            return response()->json(['status'=>'failed','responseData'=>'', 'error'=>$validate->errors()]);
        }
	}
	
	/**
	* add_visitor function call on add visitor to stand API
	* type: GET
	* param: $id stand id
	**/
	public function send_visitor_to_admin($id)
	{
		//get stand detail
		$stand = Stand::find($id);
		//get user detail
		$user = User::find($stand->company_id);
		//get visitors list	
		$visitors = VisitorLog::where('stand_id',$id)->get();
		//send email via laravel defaul mailer with template email-template
		Mail::send('email-template', ['visitors' => $visitors], function ($m) use ($user) {
            $m->from('hello@app.com', 'Your Application');

            $m->to($user->email, $user->name)->subject('Visitors log');
        });
	}
}
