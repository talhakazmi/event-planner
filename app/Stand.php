<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stand extends Model
{
    /**
     * Get the event that owns the stand.
     */
    public function event()
    {
        //return relation with event table
		return $this->belongsTo('App\Event');
    }
}
