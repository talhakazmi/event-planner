<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //Get stands of the event
	
	public function stands()
	{
		//get relation with stand table
		return $this->hasMany('App\Stand');
	}
}
