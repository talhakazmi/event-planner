<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
	private $username = 'Test User';
	private $email = 'user@test.com';
	private $password = 'qazxsw';
    /**
     * Sign up success test.
     *
     * @return void
     */
    public function testSignupSuccess()
    {
		//add user POST service call with params
        $response = $this->call('POST', 'api/signup', ['name'=>$this->username, 'email' => $this->email, 'password' => $this->password, 'password_confirmation' => $this->password]);
		//get response content in json
        $responseData   =   json_decode($response->getContent());
		
        $this->assertEquals('success', $responseData->status);
    }
	
	/**
     * Sign up failure - email already exist test.
     *
     * @return void
     */
    public function testSignupFailureAlreadyExist()
    {
		//add user POST service call with params
        $response = $this->call('POST', 'api/signup', ['name'=>$this->username, 'email' => $this->email, 'password' => $this->password, 'password_confirmation' => $this->password]);
        //get response content in json
		$responseData   =   json_decode($response->getContent());

        $this->assertEquals('failed', $responseData->status);
		//check error messag eto be corretly returned
        $this->assertEquals('The email has already been taken.', $responseData->error->email[0]);
    }
	
	/**
     * Sign up failure - name length too short test.
     *
     * @return void
     */
    public function testSignupFailureNameLengthTooShort()
    {
        //add user POST service call with invalid params
		$response = $this->call('POST', 'api/signup', ['name'=>'Test', 'email' => str_random(6).'@gmail.com', 'password' => $this->password, 'password_confirmation' => $this->password]);
        //get response content in json
		$responseData   =   json_decode($response->getContent());

        $this->assertEquals('failed', $responseData->status);
		//check error messag eto be corretly returned
        $this->assertEquals('The name must be at least 6 characters.', $responseData->error->name[0]);
    }

    /**
     * Sign up failure - password length too short test.
     *
     * @return void
     */
    public function testSignupFailurePasswordLengthTooShort()
    {
        //add user POST service call with invalid params
		$response = $this->call('POST', 'api/signup', ['name' => 'Testing', 'email' => str_random(6) . '@gmail.com', 'password' => 'qasw', 'password_confirmation' => 'qasw']);
        //get response content in json
		$responseData = json_decode($response->getContent());

        $this->assertEquals('failed', $responseData->status);
		//check error messag eto be corretly returned
        $this->assertEquals('The password must be at least 6 characters.', $responseData->error->password[0]);
    }

	/**
     * Sign up failure - confirm password does not match.
     *
     * @return void
     */
    public function testSignupFailureConfirmPasswordDoesNotMatch()
    {
        //add user POST service call with invalid params
		$response = $this->call('POST', 'api/signup', ['name' => 'Testing', 'email' => str_random(6) . '@gmail.com', 'password' => 'qazxsw', 'password_confirmation' => 'qaswcxs']);
        //get response content in json
		$responseData = json_decode($response->getContent());

        $this->assertEquals('failed', $responseData->status);
		//check error messag eto be corretly returned
        $this->assertEquals('The password confirmation does not match.', $responseData->error->password[0]);
    }
	
    /**
     * Sign in success test.
     *
     * @return void
     */
    public function testSigninSuccess()
    {
        //get user POST service call with params
        $response = $this->call('POST', 'api/signin', ['email' => $this->email, 'password' => $this->password]);
        //get response content in json
		$responseData   =   json_decode($response->getContent());

        $this->assertEquals('success', $responseData->status);
    }

    /**
     * Sign in failure test.
     *
     * @return void
     */
    public function testSigninFailure()
    {
        //get user POST service call with invalid params
		$response = $this->call('POST', 'api/signin', ['email' => 'talhaka@gmail.com', 'password' => '654321']);
        //get response content in json
		$responseData   =   json_decode($response->getContent());

        $this->assertEquals('failed', $responseData->status);
		//check error messag eto be corretly returned
		$this->assertEquals('Invalid Credentials', $responseData->error->email[0]);
		
		User::where('email', $this->email)->delete();

    }
}
