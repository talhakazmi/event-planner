<?php

namespace Tests\Unit;

use App\User;
use App\Event;
use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EventTest extends TestCase
{
	private $userId;
	private $username = 'Test User';
	private $email = 'user@test.com';
	private $password = 'qazxsw';
	private $token;
	
	/**
	* method to run before test to create a required enviornment or setup
	**/
    public function setUp(){
        parent::setUp();
		//lets register a new user to create event
		$response = $this->call('POST', 'api/signup', ['name'=>$this->username, 'email' => $this->email, 'password' => $this->password, 'password_confirmation' => $this->password]);
        $userData   =   json_decode($response->getContent());
		
		$this->userId = $userData->responseData->user->id;
		$this->token = $userData->responseData->token;
    }
	
	/**
     * Event success creation test.
     *
     * @return void
     */
    public function testEventCreationSuccess()
    {
		//Carbon class used for current datetime
		$current = Carbon::now();
		//lets add extra days in current date time and set the required formate
		$start_date = $current->addDays(1)->format('Y-m-d\TH:i');
		$end_date = $current->addDays(2)->format('Y-m-d\TH:i');
		//file uploading for add event service by picking already existing file from storage
		$file = new UploadedFile(
            storage_path() . '\app\public\event_images\not-available.jpg',
            'not-available.jpg',
            'image/jpeg',
            filesize(storage_path() . '\app\public\event_images\not-available.jpg'),
            null,
            true // for test
        );
		
		//add event POST service call with params and file to upload
        $response = $this->call('POST', 'api/event/add?token='.$this->token, [
            'user_id'=>$this->userId,
            'name'=>'Test Event 1',
            'description'=>'testign event via unit tests',
            'latitude'=>'24.35214',
            'longitude'=>'64.35214',
            'start_date'=>$start_date,
            'end_date'=>$end_date,
			'columns' => 4,
			'rows' => 3,
			''
        ], [], ['poster' => $file], ['accept' => 'application/json']);
		//get response content in json
        $responseData = json_decode($response->getContent());
		//delete the file uploaded for testing
		unlink(storage_path().'\\app\\'.$responseData->responseData->event->poster);
		
        $this->assertEquals('success', $responseData->status);
		//remove registered user to create event after test complition
		Event::find($responseData->responseData->event->id)->delete();
		//remove registered user to create event after test complition
		User::where('email', $this->email)->delete();
    }
}
